#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

__title__ ="Pluzz";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="0.3"
__usage__ ='''
Pluzz grabber for mythnetvision
> %(file)s -h
Usage: %(file)s [option][<search>]
Version: %(version)s Author: %(author)s

For details on the MythTV Netvision plugin see the wiki page at:
http://www.mythtv.org/wiki/MythNetvision

Options:
  -h, --help            show this help message and exit
  -v, --version         Display grabber name and supported options
  -S:, --search=        Search <search> for videos
  -T, --treeview        Display a Tree View of a sites videos
  -p, --page            Page of the search
'''%{'file':__file__, 'version':__version__,'author':__author__}


import sys, os, locale, re
import json
from time import time
from getopt import getopt
from xml.dom import minidom
from urllib import urlencode
from urllib2 import urlopen, Request
from datetime import date, datetime, timedelta
from ConfigParser import RawConfigParser

import pytz
tzParis = pytz.timezone ("CET")

import locale

import logging
logging.basicConfig(level=logging.DEBUG,format='%(levelname)s line %(lineno)d - %(message)s')
logger = logging.getLogger(__title__)

reload(sys)
sys.setdefaultencoding('utf-8')

# INIT
# Url de base
urlSite = 'http://pluzz.francetv.fr'
urlBase = 'http://pluzz.francetv.fr%s'
urlList = 'http://pluzz.webservices.francetelevisions.fr/pluzz/liste/type/replay/nb/10000/debut/0'
urlSearch = 'http://pluzz.webservices.francetelevisions.fr/pluzz/recherche/requete/%(text)s/tri/pertinence/nb/%(nb)s/debut/%(start)s'
nbBySearch = 20
urlImage = 'http://refonte.webservices.francetelevisions.fr%s'

# Description de channel
channel = {
        'title' : __title__,
        'link' : urlSite,
        'description' : 'Voir ou revoir les programmes de france télévisions'
    }


def MinidomDocument_createNode(self,tag,text='',attributes={}) :
    '''
        Retourne un noeud avec pour tag "tag", comme text "text" ou comme
        attributs le dico "attributes" ({attrName: attrValue})
    '''
    node = self.createElement(tag)
    if text :
        node.appendChild(self.createTextNode(text))
    for (name,value) in attributes.items() :
        node.setAttribute(name,value)
    return node
setattr(minidom.Document,'createNode',MinidomDocument_createNode)


def MinidomDocument_itemNode(self,dictVideo,titlesubtitle=True) :
    '''
        Converti la page de item du site de canal en item Mythnetvision
    '''
    itemOutputNode = self.createNode('item')
    pubDate = datetime.strptime(dictVideo['date_diffusion'], '%Y-%m-%dT%H:%M')
    now = date.today()
    if now.day == pubDate.day :
        prettyFpubDate = pubDate.strftime('aujourd\'hui à %Hh%M')
    elif (now - timedelta(1)).day == pubDate.day :
        prettyFpubDate = pubDate.strftime('hier à %Hh%M')
    else :
        oldlocale = locale.getlocale()
        locale.setlocale(locale.LC_ALL,'')
        prettyFpubDate = pubDate.strftime('le %A %d %B à %Hh%M')
        locale.setlocale(locale.LC_ALL,oldlocale)
    finishDate = float(dictVideo['temps_restant'])
    if finishDate < 90 :
        '''Reste moins d'1.5min'''
        prettyFinishDate = '1 minute'
    elif finishDate < 7200 :
        '''Reste moins d'2h'''
        prettyFinishDate = '%i minutes'%round(finishDate/60)
    elif finishDate < 172800 :
        '''Reste moin de 2j'''
        prettyFinishDate = '%i heures'%round(finishDate/3600)
    else :
        prettyFinishDate = '%i jours'%round(finishDate/86400)
    if titlesubtitle :
        itemOutputNode.appendChild(self.createNode('title',dictVideo['titre']+' - '+dictVideo['soustitre']))
    else :
        itemOutputNode.appendChild(self.createNode(dictVideo['soustitre']))
    itemOutputNode.appendChild(self.createNode('mythtv:subtitle',dictVideo['soustitre']))
    itemOutputNode.appendChild(self.createNode('author',dictVideo['chaine_label']))
    itemOutputNode.appendChild(self.createNode('pubDate',pubDate.replace(tzinfo = tzParis).strftime('%a, %d %b %Y %H:%M:%S %z')))
    itemOutputNode.appendChild(self.createNode('description',(\
            'Diffusé '+prettyFpubDate+' (plus que '+prettyFinishDate+' en replay)'+"\n\n"+\
            dictVideo['accroche_programme']+"\n\n"+\
            dictVideo['accroche']).strip())
        )
    itemOutputNode.appendChild(self.createNode('link',urlBase%dictVideo['url']))
    itemOutputNode.appendChild(self.createNode('duration','%i'%(int(dictVideo['duree'])*60)))
    mediaOutputNode = itemOutputNode.appendChild(self.createNode('media:group'))
    mediaOutputNode.appendChild(self.createNode(tag='media:thumbnail', attributes={'url': urlBase%dictVideo['image_large']}))
    feed = dictVideo['url_video_sitemap']
    feed = feed.replace('http://medias2.francetv.fr/mp4google/', 'http://ftvodhdsecz-f.akamaihd.net/i/') #mmmh mhouais bof
    feed = feed.replace('398k.mp4', ',398,632,934,k.mp4.csmil/master.m3u8')
    mediaOutputNode.appendChild(self.createNode(tag='media:content',   attributes={'url':feed}))
    itemOutputNode.appendChild(self.createNode('mythtv:season',dictVideo['saison']))
    itemOutputNode.appendChild(self.createNode('mythtv:episode',dictVideo['episode']))
    return itemOutputNode
setattr(minidom.Document,'createItemNode',MinidomDocument_itemNode)


def domVersion() :
    '''
        Retourne le noeux racine permettant a mythtv de reconnaitre le nom,
        et les fonction de ce grabbeur
    '''
    domOutput = minidom.Document()
    rootNode = domOutput.appendChild(domOutput.createNode('grabber'))
    rootNode.appendChild(domOutput.createNode('name',__title__))
    rootNode.appendChild(domOutput.createNode('command',__file__))
    rootNode.appendChild(domOutput.createNode('author',__author__))
    rootNode.appendChild(domOutput.createNode('thumbnail','pluzz.png'))
    rootNode.appendChild(domOutput.createNode('type','video'))
    rootNode.appendChild(domOutput.createNode('description',channel['description']))
    rootNode.appendChild(domOutput.createNode('version',__version__))
    rootNode.appendChild(domOutput.createNode('search','true'))
    rootNode.appendChild(domOutput.createNode('tree','true'))
    return rootNode


def domTreeview() :
    '''
        Retourne un domDocument pour la vue arbre de MythNetVision
    '''

    # dom xml de sortie
    domOutput = minidom.Document()
    nodeOutputRss = domOutput.appendChild(domOutput.createNode(
            tag='rss',
            attributes={
                    'version':'2.0',
                    'xmlns:itunes':'http://www.itunes.com/dtds/podcast-1.0.dtd',
                    'xmlns:content':'http://purl.org/rss/1.0/modules/content/',
                    'xmlns:cnettv':'http://cnettv.com/mrss/',
                    'xmlns:creativeCommons':'http://backend.userland.com/creativeCommonsRssModule',
                    'xmlns:media':'http://search.yahoo.com/mrss/',
                    'xmlns:atom':'http://www.w3.org/2005/Atom',
                    'xmlns:amp':'http://www.adobe.com/amp/1.0',
                    'xmlns:dc':'http://purl.org/dc/elements/1.1/',
                }
        ))

     # node channel
    channelOutputNode = nodeOutputRss.appendChild(domOutput.createNode('channel'))
    channelOutputNode.appendChild(domOutput.createNode('title',__title__))
    channelOutputNode.appendChild(domOutput.createNode('link',channel['link']))
    channelOutputNode.appendChild(domOutput.createNode('description',channel['description']))

    emissions = json.loads(urlopen(urlList).read())['reponse']['emissions']
    #~ emissions = json.loads(open('list.json').read())['reponse']['emissions'] #test avec fichier json local

    outputNodeChaines = {}
    outputNodeRubriques = {}
    outputNodeEmissions = {}

    for emission in emissions :
        if emission['chaine_id'] not in outputNodeChaines.keys() :
            '''On créer le directory "chaine" si il n'existe pas'''
            outputNodeChaines[emission['chaine_id']] = domOutput.createNode(
                    tag = 'directory',
                    attributes = {
                            'name': emission['chaine_label'],
                            'thumbnail': 'http://static.francetv.fr/arches/%(chaine_id)s/default/img/logo_%(chaine_id)s.png'%emission
                        }
                )
            channelOutputNode.appendChild(outputNodeChaines[emission['chaine_id']])

            outputNodeRubriques[emission['chaine_id']] = {}
            outputNodeEmissions[emission['chaine_id']] = {}

        if emission['rubrique'] not in outputNodeRubriques[emission['chaine_id']].keys() :
            '''On créer le directory "rubrique" si il n'existe pas'''
            outputNodeRubriques[emission['chaine_id']][emission['rubrique']] = domOutput.createNode(
                    tag = 'directory',
                    attributes = {
                            'name': emission['rubrique'],
                            'thumbnail': urlImage%emission['image_large']
                        }
                )
            outputNodeChaines[emission['chaine_id']].appendChild(outputNodeRubriques[emission['chaine_id']][emission['rubrique']])

            outputNodeEmissions[emission['chaine_id']][emission['rubrique']] = {}

        if emission['titre'] not in outputNodeEmissions[emission['chaine_id']][emission['rubrique']].keys() :
            '''On créer le directory "emissions" si il n'existe pas'''
            outputNodeEmissions[emission['chaine_id']][emission['rubrique']][emission['titre']] = domOutput.createNode(
                    tag = 'directory',
                    attributes = {
                            'name': emission['titre'],
                            'thumbnail': urlImage%emission['image_large']
                        }
                )
            outputNodeRubriques[emission['chaine_id']][emission['rubrique']].appendChild(outputNodeEmissions[emission['chaine_id']][emission['rubrique']][emission['titre']])

        outputNodeEmissions[emission['chaine_id']][emission['rubrique']][emission['titre']].appendChild(domOutput.createItemNode(emission))

    return domOutput


def domSearch (text,page) :
    '''
        Retourne un domDocument pour la recherche de MythNetVision
    '''

    domOutput = minidom.Document()
    nodeOutputRss = domOutput.appendChild(domOutput.createNode(
            tag='rss',
            attributes={
                    'version':'2.0',
                    'xmlns:itunes':'http://www.itunes.com/dtds/podcast-1.0.dtd',
                    'xmlns:content':'http://purl.org/rss/1.0/modules/content/',
                    'xmlns:cnettv':'http://cnettv.com/mrss/',
                    'xmlns:creativeCommons':'http://cnettv.com/mrss/',
                    'xmlns:media':'http://search.yahoo.com/mrss/',
                    'xmlns:atom':'http://www.w3.org/2005/Atom',
                    'xmlns:amp':'http://www.adobe.com/amp/1.0',
                    'xmlns:dc':'http://purl.org/dc/elements/1.1/',
                    'xmlns:mythtv':'http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format'
                }))

    channelOutputNode = nodeOutputRss.appendChild(domOutput.createNode('channel'))
    channelOutputNode.appendChild(domOutput.createNode('title',__title__))
    channelOutputNode.appendChild(domOutput.createNode('link',channel['link']))
    channelOutputNode.appendChild(domOutput.createNode('description',channel['description']))

    logger.debug('Url recherche : '+ urlSearch%{'text':text, 'nb':nbBySearch, 'start': (page-1)*nbBySearch})
    reponse = json.loads(urlopen(urlSearch%{'text':text, 'nb':nbBySearch, 'start': page*nbBySearch}).read())['reponse']
    channelOutputNode.appendChild(domOutput.createNode('numresults',"%i"%reponse['total']))
    channelOutputNode.appendChild(domOutput.createNode('returned',"%i"%reponse['nb']))

    for emission in reponse['emissions'] :
        channelOutputNode.appendChild(domOutput.createItemNode(emission))

    return domOutput


if __name__ == "__main__" :
    opts,args = getopt(sys.argv[1:], "hvS:Tp:",["help","version","search=","treeview","page="])
    if len(opts) == 0 :
        sys.stdout.write(__usage__)
        sys.exit(0)

    search = False
    page = 1
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            sys.stdout.write(__usage__)
            sys.exit(0)
        elif opt in ("-v", "--version"):
            sys.stdout.write(domVersion().toxml())
            sys.exit(0)
        elif opt in ("-S", "--search"):
            search = arg
        elif opt in ("-p", "--page"):
            page = int(arg)
        elif opt in ("-T", "--treeview"):
            sys.stdout.write(domTreeview().toxml())
            sys.exit(0)

    if search :
        sys.stdout.write(domSearch(search,page).toxml())
        sys.exit(0)
